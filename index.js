/**
 * @format
 */

import {AppRegistry} from 'react-native';

import {name as appName} from './app.json';

import AppRouter from './src/Router/AppRouter'
import Store from './src/Data/Store'
import Screen from './src/Task/Screen'
import Cake from './src/Task/Cake'
import Splashscreen from './src/Pages/Splashscreen'
import CakeAppRouter from './src/Task/CakeRouter/CakeAppRouter'
import OnBoarding from './src/Pages/OnBoarding'
import Flatlist from './src/Pages/Flatlist'
import Svglist from './src/Task/Svglist';

import List from './src/JananiAssignment1/List'
import Welcome from './src/Design/Welcome'
import DesignAppRouter from './src/Design/DesignAppRouter'
import Home from './src/Design/Home';




AppRegistry.registerComponent(appName, () => DesignAppRouter);
