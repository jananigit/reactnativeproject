import React, { useState } from 'react';
import { StyleSheet, Text,  View,  TouchableOpacity,  Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { AuthenticationStyles } from '../Styles/GlobalStyles';

const Store = () => {

    const [result,setresult] = useState();
    const SetItem = async () => {
        await AsyncStorage.setItem('data','Siam Computing')
    }

    const GetItem = async () => {
       const store = await AsyncStorage.getItem('data')
        Alert.alert('Welcome to',store)
        setresult(store);
      
    }

    const Styles = AuthenticationStyles();

    return (
        <View style={styles.container}>
           <Text style={{color:'red',fontSize:25}}>Welcome to {result}</Text>
            <TouchableOpacity onPress={SetItem}>
                <Text style={[Styles.button, { marginTop: 45, color: 'white', backgroundColor: 'blue' }]}>Set Item</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={GetItem}>
                <Text style={[Styles.button, { marginTop: 45, color: 'white', backgroundColor: 'blue' }]}>Get Item</Text>
            </TouchableOpacity>

        </View>

    )
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
   
});

export default Store;