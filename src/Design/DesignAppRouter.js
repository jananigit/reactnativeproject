import React from 'react'; 
import { NavigationContainer } from '@react-navigation/native';

import { Provider } from 'react-redux';
import { Store } from '../Redux/Store';



import DesignRootStackScreen from './DesignRootStackScreen';

const AppRouter = () => {

    return(
        <Provider store={Store}>
        <NavigationContainer>
            <DesignRootStackScreen />
        
        </NavigationContainer>
        </Provider>

        
    );
};


export default AppRouter;