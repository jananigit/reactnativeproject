import React from 'react'; 
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Welcome from './Welcome';
import LoginPage from './LoginPage';
import SigninPage from './SigninPage';
import Home from './Home';

const RootStack = createNativeStackNavigator();

const DesignRootStackScreen = ({ navigation }) => (
    <RootStack.Navigator screenOptions={{headerShown: false}} initialRouteName="Welcome">
       
        <RootStack.Screen name="Welcome" component={Welcome} />
        <RootStack.Screen name="LoginPage" component={LoginPage} />
        <RootStack.Screen name="SigninPage" component={SigninPage} />
        <RootStack.Screen name="Home" component={Home} />
        
       
        
    </RootStack.Navigator>
);

export default DesignRootStackScreen;