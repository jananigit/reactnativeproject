import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView, TextInput } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {  useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import { setName, removeName } from '../Redux/Action/Action';


const Home = () => {
const [name,setName] = useState('')




const dispatch = useDispatch();
    return (
        <View style={styles.container} >
            <View style={{ marginTop: 170 }}>
                <Text style={{ fontSize: 22, color: 'black' }}>Enter Name</Text>
                <TextInput
                    style={styles.textInput}
                  
               onChangeText={(value) => dispatch(setName(value))}
                

                />
            </View>
            <View>
                <TouchableOpacity  style={[styles.button, { marginVertical: 20 }]}>
                    <Text style={{ fontSize: 20, color: 'black' }}>Submit</Text>
                </TouchableOpacity>
            </View>
            <View style={{ marginTop: 100 }}>
                <Text style={{ fontSize: 22, color: 'black' }}>Output</Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textInput}>{}</Text>
                    <TouchableOpacity onPress={(name) => removeName(name)}>
                        <MaterialCommunityIcons name="close-circle" size={28} color='black' style={{ marginTop: 22, marginLeft: 10 }} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => {
return{
    name:state.name
};
};

const mapDispatchToProps = (dispatch) => {
return{
    setName:name => dispatch(setName(name)),
    removeName:name => dispatch(removeName(name))
};
};


export default connect(mapStateToProps,mapDispatchToProps)(Home);


const styles = StyleSheet.create({
    container: {
        flex: 1,

        alignItems: 'center',
        backgroundColor: '#fff'
    },

    textInput: {
        marginTop: 15,
        width: 280,
        padding: 12,
        borderWidth: 1,
        marginVertical: 20

    },
    button: {
        borderWidth: 1,

        borderRadius: 5,
        width: 200,
        height: 50,
        textAlign: 'center',
        padding: 10,
        justifyContent: 'center',
        fontSize: 16,
        alignItems: 'center',

        //borderStyle:'double'


    },


});