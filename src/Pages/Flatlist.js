import React, {  useEffect, useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, FlatList, Image, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { AuthenticationStyles } from '../Styles/GlobalStyles';



const Flatlist = ({ navigation }) => {


  const validation = () => {
    auth()
      .signOut()
      .then(() => {
        navigation.navigate('Hello');
      });


  }



  const [data, setData] = useState([])


  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(flatd =>
        setData(flatd)
        //console.log(flatd)
      );

  }, []);

  const AddItem = () => {

    fetch('https://jsonplaceholder.typicode.com/posts', {

      method: 'POST',
      body: JSON.stringify({

        title: 'Elon Musk',
        body: 'elonmusk@gmail.com',
      }),
      headers: { "Content-Type": "application/json; charset=utf-8" },
    })
      .then(response => response.json())
      .then(flatd =>
        // console.log(flatd)
        setData(flatd)
      );


  };

  const UpdateItem = () => {

    fetch('https://jsonplaceholder.typicode.com/posts/1', {

      method: 'PUT',
      body: JSON.stringify({
        userId: 1,
        id: 1,
        title: 'Elon Musk',
        body: 'elonmusk@gmail.com',
      }),
      headers: { "Content-Type": "application/json; charset=utf-8" },
    })
      .then(response => response.json())
      .then(flatd =>
        //console.log(flatd),
        setData(flatd)
        //setData([...data,flatd])
      );





  };

  const DeleteItem = () => {
    fetch('https://jsonplaceholder.typicode.com/posts/0', {
      method: 'DELETE'
    })
      .then(response => response.json())
      .then(flatd =>
        //console.log(flatd),
        setData(flatd)
      );
  };

  const Styles = AuthenticationStyles();

  return (
    <SafeAreaView style={Styles.container}>

      <FlatList
        data={Object.values(data)}


        renderItem={({ item }) => (

          <SafeAreaView style={styles.container}>
            <View>
              <View style={{
                backgroundColor: 'white',
                padding: 7,
                marginVertical: 7,
                marginHorizontal: 16,
                justifyContent: 'center',
                borderRadius: 8,
                borderWidth: 2,
                borderColor: '#534A4C',
              }}>
                <Text style={{ fontSize: 15, fontWeight: '600', color: 'dodgerblue', textAlign: 'justify', marginTop: 7, }}> {item.id}.Title: {item.title}</Text>
                <Text style={{ fontSize: 15, color: 'red', fontWeight: '500', marginTop: 10, textAlign: 'justify' }}>Body:  {item.body} </Text>



              </View>
            </View>
          </SafeAreaView>




        )}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        extraData={data}



      />
      <TouchableOpacity onPress={AddItem} activeOpacity={0.4} style={{ marginHorizontal: 60, justifyContent: 'center' }}>
        <Text style={[Styles.button, { marginVertical: 10, color: 'white', backgroundColor: 'blue' }]}>ADD ITEM</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={UpdateItem} activeOpacity={0.4} style={{ marginHorizontal: 60, justifyContent: 'center' }}>
        <Text style={[Styles.button, { marginVertical: 10, color: 'white', backgroundColor: 'blue' }]}>UPDATE ITEM</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={DeleteItem} activeOpacity={0.4} style={{ marginHorizontal: 60, justifyContent: 'center' }}>
        <Text style={[Styles.button, { marginVertical: 10, color: 'white', backgroundColor: 'blue' }]}>DELETE ITEM</Text>
      </TouchableOpacity>

      {/* <TouchableOpacity onPress={validation} activeOpacity={0.4} style={{ marginHorizontal: 60, justifyContent: 'center' }}>
        <Text style={[Styles.button, { marginVertical: 10,  color: 'white', backgroundColor: 'blue' }]}>SIGN OUT</Text>
            </TouchableOpacity> */}

    </SafeAreaView>
  );

}

const styles = StyleSheet.create({
  
  item: {
    backgroundColor: 'white',
    padding: 20,
    marginVertical: 10,
    marginHorizontal: 16,
    justifyContent: 'center',
    borderRadius: 8,
    borderWidth: 2,
    borderColor: 'black',
  },
  
});

export default Flatlist;