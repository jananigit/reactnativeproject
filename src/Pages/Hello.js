import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView } from 'react-native';

import { AuthenticationStyles } from '../Styles/GlobalStyles';


const Hello = ({ navigation }) => {


    const Styles = AuthenticationStyles();


    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1 }}>
                <Image
                    style={{ height: 350, width: 320, }}
                    source={require('../../assets/img.png')}
                />


            </View>
            <View style={styles.txtContainer}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: 'black' }}>Hello !</Text>
                <Text style={{ fontSize: 15, marginTop: 20, textAlign: 'center', color: 'black' }}>Best place to write life stories and  share your journey experiences</Text>


                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={[Styles.button, { marginTop: 45, color: 'white', backgroundColor: 'blue' }]}>LOGIN</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
                    <Text style={[Styles.button, { marginTop: 20, color: 'blue', }]}>SIGNUP</Text>
                </TouchableOpacity>
            </View>





        </SafeAreaView>
    );
}

export default Hello;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#fff'
    },
    txtContainer: {

        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 50,
        marginHorizontal: 50
    },


});