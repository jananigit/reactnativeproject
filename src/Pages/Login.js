import React from 'react';
import { StyleSheet, Text, TextInput, View, SafeAreaView, TouchableOpacity, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';

import { AuthenticationStyles } from '../Styles/GlobalStyles';


const Login = ({ navigation }) => {


    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')

    const validation = () => {

        if (email === '') {
            Alert.alert('Email field cannot be Empty!')
        } else if (password === '') {
            Alert.alert('Password field cannot be Empty!')
        } else {
            auth()
                .signInWithEmailAndPassword(email, password)
                .then(() => {
                    navigation.navigate('Flatlist');
                })
                .catch(error => {
                    if (error.code === 'auth/wrong-password') {
                        console.log('Email address/Password is incorrect!');
                        Alert.alert('Email address/Password is incorrect!');
                    }
                    if (error.code === 'auth/user-not-found') {
                        console.log('Email is not registered!');
                        Alert.alert('Email is not registered!');
                    }

                    if (error.code === 'auth/invalid-email') {
                        console.log('That email address is invalid!');
                        Alert.alert('That email address is invalid!');
                    }

                    console.error(error);
                })
        }
    }

    const Styles = AuthenticationStyles();

    return (
        <SafeAreaView style={Styles.container}>
            <KeyboardAwareScrollView>

                <AntDesign name="arrowleft" size={30} color='black' style={{ margin: 17 }} onPress={() => { navigation.goBack() }} />
                <View style={{ margin: 30 }}>
                    <Text style={{ fontSize: 35, fontWeight: 'bold', color: 'black', marginTop: -9 }}>Welcome !</Text>
                    <Text style={{ fontSize: 17, marginTop: 5 }}>Sign in to continue</Text>
                    <TextInput
                        placeholder="Your Email"
                        style={Styles.textInput}
                        autoCapitalize='none'
                        keyboardType='email-address'
                        onChangeText={(text) => setEmail(text)}

                    />
                    <TextInput
                        placeholder="Password"
                        style={Styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={true}
                        onChangeText={(text) => setPassword(text)}



                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={validation} activeOpacity={0.4}>
                        <Text style={[Styles.button, { marginTop: 45, color: 'white', backgroundColor: 'blue' }]}>LOGIN</Text>
                    </TouchableOpacity>
                    <Text style={{ marginTop: 10 }}>Forgot Password ?</Text>

                    <Text style={{ fontSize: 17, marginTop: 60 }}>Social Media Login</Text>

                    <Image
                        style={{ width: 55, height: 55, marginTop: 15 }}
                        source={require('../../assets/google.png')}
                    />

                    <Text>
                        <Text style={{ margin: 15 }}>Dont have an account yet?</Text>
                        <Text onPress={() => navigation.navigate('Signup')} style={{ fontWeight: '700', color: 'dodgerblue' }} >  Sign up</Text></Text>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>



    );
}
export default Login;





