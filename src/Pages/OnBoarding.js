import React, { useEffect } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { Avatar } from 'react-native-paper'

import AppIntroSlider from 'react-native-app-intro-slider';
import MainTabScreen from '../Task/CakeRouter/MainTabScreen';
import { NavigationContainer } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RootStackScreen from '../Router/RootStackScreen';







const slides = [
    {
        key: 's1',
        title: 'Order Your Favourite Food',
        image: {
            uri: 'https://media.istockphoto.com/vectors/ordering-food-using-online-mobile-application-vector-id1170763624?k=20&m=1170763624&s=612x612&w=0&h=LwdvPOtSRnei5UNiRWaCnF7V_Y_tyn_7cZnKRCssfDs='
        },
        backgroundColor: '#1CBEB1',
    },
    {
        key: 's2',
        title: 'Fast Delivery',
        image: {
            uri: 'https://www.crushpixel.com/big-static15/preview4/food-delivery-service-scooter-with-2004994.jpg'
        },
        backgroundColor: '#1C66E4',
    },
    {
        key: 's3',
        title: 'Good Service',
        image: {
            uri: 'https://media.istockphoto.com/vectors/happy-family-receiving-a-ready-meal-at-home-using-a-smartphone-app-vector-id1152170048?k=20&m=1152170048&s=612x612&w=0&h=F4awIhgJ88BO3KY-98RNPPhnu6bEpUx5R7tPhj-3YXQ='
        },
        backgroundColor: '#9F1CE4',
    },

];



const OnBoarding = () => {
    const [showRealApp, setShowRealApp] = React.useState(false);

    const [isFirstLaunch, setIsFirstLaunch] = React.useState(null);

    useEffect(() => {
        AsyncStorage.getItem('alreadyLaunched').then(value => {
            if (value == null) {
                AsyncStorage.setItem('alreadyLaunched', 'true');
                setIsFirstLaunch(true);
            } else {
                setIsFirstLaunch(false);
            }
        });
    }, []);


    const onDone = () => {
        setShowRealApp(true)
    };

    const onSkip = () => {
        setShowRealApp(true)
    };

    const renderDoneButton = () => {
        return (
            <View>
                <View style={{ flexDirection: 'row', borderColor: 'white', borderWidth: 2, borderRadius: 40, padding: 5, paddingHorizontal: 10, paddingTop: 10, backgroundColor: 'white' }}>
                    <View>
                        <Text style={{ color: '#9F1CE4', fontSize: 15, fontWeight: '500' }}>Get Started</Text>
                    </View>
                    <View>
                        <MaterialIcons
                            name="keyboard-arrow-right"
                            color="#9F1CE4"
                            size={24}
                            style={{ backgroundColor: 'transparent' }}
                        />
                    </View>
                </View>
            </View>
        );
    }

    const RenderItem = ({ item }) => {
        return (
            <View style={{
                flex: 1,
                backgroundColor: item.backgroundColor,
                alignItems: 'center',
                justifyContent: 'center',
                paddingBottom: 100,
            }}>

                <Avatar.Image
                    size={250}
                    style={{ marginBottom: 40, marginTop: 80 }}
                    source={item.image} />
                <Text style={styles.introTitleStyle}>
                    {item.title}
                </Text>

            </View>

        );
    };

    if (isFirstLaunch === null) {
        return null;
    } else if (isFirstLaunch === true) {
        return (
            <NavigationContainer>
                {showRealApp ? (
                    <MainTabScreen />

                ) : (
                    <AppIntroSlider
                        data={slides}
                        renderItem={RenderItem}
                        onDone={onDone}
                        showSkipButton={true}
                        onSkip={onSkip}
                        renderDoneButton={renderDoneButton}

                    />
                )}
            </NavigationContainer>
        );
    } else {

        return (
            <NavigationContainer>
                <MainTabScreen />
            </NavigationContainer>
        );

    }



};

export default OnBoarding;

const styles = StyleSheet.create({
    introTitleStyle: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        margin: 16,
        fontWeight: 'bold',
        marginTop: 30
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
    },
    titleStyle: {
        padding: 10,
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
    },
    paragraphStyle: {
        padding: 20,
        textAlign: 'center',
        fontSize: 16,
    },
});
