import React, { Component } from 'react';
import {  Text, TextInput, View, SafeAreaView, TouchableOpacity, Image, Alert } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';

import { AuthenticationStyles } from '../Styles/GlobalStyles';


const Signup = ({ navigation }) => {

    const [name, setName] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')

    const validation = () => {

        if (name === '') {
            Alert.alert('Name field cannot be Empty!')
        } else if (email === '') {
            Alert.alert('Email field cannot be Empty!')
        } else if (password === '') {
            Alert.alert('Password field cannot be Empty!')
        } else {
            auth()
                .createUserWithEmailAndPassword(email, password)
                .then(() => {
                    navigation.navigate('Flatlist');
                })
                .catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        console.log('That email address is already in use!');
                        Alert.alert('That email address is already in use!');
                    }

                    if (error.code === 'auth/invalid-email') {
                        console.log('That email address is invalid!');
                        Alert.alert('That email address is invalid!');
                    }

                    console.error(error);

                })
        }

    }

    const Styles = AuthenticationStyles();

    return (
        <SafeAreaView style={Styles.container}>
            <KeyboardAwareScrollView>
                <AntDesign name="arrowleft" size={30} color='black' style={{ margin: 17, }} onPress={() => { navigation.goBack() }} />
                <View style={{ margin: 30 }}>
                    <Text style={{ fontSize: 35, fontWeight: 'bold', color: 'black', marginTop: -10 }}>Hi !</Text>
                    <Text style={{ fontSize: 17, marginTop: 5 }}>Create a new account</Text>
                    <TextInput
                        placeholder="Your Name"
                        style={Styles.textInput}
                        onChangeText={(text) => setName(text)}

                    />
                    <TextInput
                        placeholder="Your Email"
                        style={Styles.textInput}
                        autoCapitalize='none'
                        keyboardType='email-address'
                        onChangeText={(text) => setEmail(text)}
                    />
                    <TextInput
                        placeholder="Password"
                        style={Styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={true}
                        onChangeText={(text) => setPassword(text)}
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={validation} activeOpacity={0.4}>
                        <Text style={[Styles.button, { marginTop: 45, fontSize: 16, color: 'white', backgroundColor: 'blue' }]}>SIGN UP</Text>
                    </TouchableOpacity>


                    <Text style={{ fontSize: 17, marginTop: 35 }}>Social Media Signup</Text>

                    <Image
                        style={{ width: 55, height: 55, marginTop: 15 }}
                        source={require('../../assets/google.png')}
                    />

                    <Text>
                        <Text style={{ margin: 15 }}>Already have an account ? </Text>
                        <Text onPress={() => navigation.navigate('Login')} style={{ fontWeight: '700', color: 'dodgerblue' }}>  Sign in</Text>
                    </Text>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>


    );
};

export default Signup;