import React from 'react';
import { StyleSheet, Text, View,  Image, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';


const Splashscreen = ({ navigation }) => {
    setTimeout(() => {
        navigation.replace('Hello');

    }, 3000);


    return (
        <View>
            <ImageBackground
                style={styles.container}
                source={require('../../assets/splash4.png')}  >
                <Animatable.Image
                    animation="bounceInDown"
                    duration={2000}
                    source={require('../../assets/appiconup.png')}
                    style={{ width: 120, height: 120, borderRadius: 25, borderWidth: 2, borderColor: "#1d0554" }}
                />
                <Animatable.Text
                    animation="slideInLeft"
                    duration={2600}
                    style={{ marginTop: 15, fontWeight: '500', fontSize: 25, fontFamily: 'times', fontStyle: 'italic', color: 'white', textShadowRadius: 10, textShadowColor: '#0a10f2', textShadowOffset: { width: -1, height: -1 } }}>Welcome !</Animatable.Text>
            </ImageBackground>
        </View>



    );
};


export default Splashscreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',


    },
});