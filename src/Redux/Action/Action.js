export const SET_USER_NAME = 'SET_USER_NAME';
export const REMOVE_NAME = 'REMOVE_NAME';

export const setName = name => dispatch => {
    dispatch({
        type: SET_USER_NAME,
        payload: name,
    });
};

export const removeName = name => dispatch => {
    dispatch({
        type: REMOVE_NAME,
        payload: name,
    });
};