import { SET_USER_NAME,REMOVE_NAME } from '../Action/Action';

const initialState = {
    name: '',
}


function userReducer(state = initialState, action) {
    switch (action.type) {
        case SET_USER_NAME:
            return { ...state, name: action.payload };
        
        case REMOVE_NAME:
            return { ...state, name: state.name.filter(name => name !==action.payload) };
        default:
            return state;
    }
}

export default userReducer;