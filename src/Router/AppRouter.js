import React, {Component} from 'react'; 
import { NavigationContainer } from '@react-navigation/native';

import RootStackScreen from '../Router/RootStackScreen'
import CakeAppRouter from '../Task/CakeRouter/CakeAppRouter';

const AppRouter = () => {

    return(
        <NavigationContainer>
            <RootStackScreen />
            
            
        </NavigationContainer>

        
    );
};


export default AppRouter;
