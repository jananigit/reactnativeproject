import React from 'react'; 
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Hello from '../Pages/Hello'
import Login from '../Pages/Login'
import Signup from '../Pages/Signup'
import Flatlist from '../Pages/Flatlist';
import Splashscreen from '../Pages/Splashscreen';
import MainTabScreen from '../Task/CakeRouter/MainTabScreen';
import CakeAppRouter from '../Task/CakeRouter/CakeAppRouter';


const RootStack = createNativeStackNavigator();

const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator screenOptions={{headerShown: false}} initialRouteName="Splashscreen">
        <RootStack.Screen name="Splashscreen" component={Splashscreen} />
        <RootStack.Screen name="Hello" component={Hello} />
        <RootStack.Screen name="Login" component={Login} />
        <RootStack.Screen name="Signup" component={Signup} />
        <RootStack.Screen name="Flatlist" component={Flatlist} />
       
        
    </RootStack.Navigator>
);

export default RootStackScreen;