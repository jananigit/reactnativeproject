import { StyleSheet } from "react-native";


const AuthenticationStyles = () =>
    StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff'
        },
        button: {
            borderWidth: 2,
            borderColor: 'blue',
            borderRadius: 5,
            width: 240,
            height: 40,
            textAlign: 'center',
            padding: 10,
            fontWeight: '600',
            fontSize: 16,

        },
        textInput: {
            marginTop: 50,
            width: 260,
            padding: 10,
            borderBottomWidth: 1,
            borderColor: 'grey'
        },
    });

export { AuthenticationStyles };