import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';



const Cake = () => {
    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
           
            <View style={{ flex: 0.5, marginLeft: 30, marginTop: 25 }}>
                <Text style={{ fontSize: 27, fontWeight: 'bold', color: 'black', }}>Fresh taste of</Text>
                <Text style={{ fontSize: 27 }}>Designer cakes</Text>
            </View>
            <View style={{ flexDirection: 'row', flex: 2.75, alignItems: 'flex-start' }}>
                <View>

                    <TouchableOpacity style={styles.item}>

                        <Image
                            resizeMode='contain'
                            style={{ height: 140, width: 142, borderTopRightRadius: 40, borderTopLeftRadius: 45, margin: 2 }}
                            source={require('../../assets/cold.png')}
                        />
                        <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>Cold Coffee</Text>
                        <Text style={{ marginLeft: 10 }}>Lime with Coffee</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>$24.00</Text>
                            <MaterialCommunityIcons name="heart-outline" size={20} color='black' style={{ marginLeft: 50, marginTop: 5 }} />
                        </View>

                    </TouchableOpacity>



                    <TouchableOpacity style={styles.item}>
                        <Image
                            resizeMode='contain'
                            style={{ height: 120, width: 135, borderTopRightRadius: 40, borderTopLeftRadius: 45, margin: 5 }}
                            source={require('../../assets/straw.png')}
                        />
                        <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>Strawberry Cake</Text>
                        <Text style={{ marginLeft: 10 }}>Cream with Strawberry</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>$12.00</Text>
                            <MaterialCommunityIcons name="heart-outline" size={20} color='black' style={{ marginLeft: 50, marginTop: 5 }} />
                        </View>

                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.itemrow}>
                        <Image
                            resizeMode='stretch'
                            style={{ height: 100, width: 149, borderTopRightRadius: 40, borderTopLeftRadius: 45 }}
                            source={require('../../assets/blued.png')}
                        />
                        <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>Blueberry Cake</Text>
                        <Text style={{ marginLeft: 10 }}>Cream with berry</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>$65.00</Text>
                            <MaterialCommunityIcons name="heart-outline" size={20} color='black' style={{ marginLeft: 50, marginTop: 5 }} />
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity style={styles.itemrow}>
                        <Image
                            resizeMode='contain'
                            style={{ height: 100, width: 130, borderTopRightRadius: 40, borderTopLeftRadius: 45, margin: 2 }}
                            source={require('../../assets/hot.png')}
                        />
                        <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>Hot Coffee</Text>
                        <Text style={{ marginLeft: 10 }}>Fresh Coffee</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ marginTop: 5, marginLeft: 10, color: 'black', fontWeight: '600' }}>$30.00</Text>
                            <MaterialCommunityIcons name="heart-outline" size={20} color='black' style={{ marginLeft: 50, marginTop: 5 }} />
                        </View>

                    </TouchableOpacity>
                    <View style={[styles.button, { flexDirection: 'row', backgroundColor: 'lightseagreen', paddingTop: 10 }]}>

                        <Text style={{ justifyContent: 'center', color: 'white', justifyContent: 'center', fontSize: 17 }}>View More </Text>
                        <View style={{ backgroundColor: 'white', borderRadius: 50, height: 30, width: 30, alignItems: 'center' }}>
                            <MaterialIcons name="keyboard-arrow-right" size={28} color='lightseagreen' style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 5 }} />
                        </View>

                    </View>
                </View>

            </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>

    )
};

export default Cake;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lavender'



    },
    item: {
        backgroundColor: 'white',
        borderRadius: 25,
        height: 220,
        width: 150,
        marginVertical: 10,
        marginLeft: 20
    },
    itemrow: {
        backgroundColor: 'white',
        borderRadius: 25,
        height: 180,
        width: 150,
        marginVertical: 10,
        marginHorizontal: 20,
        marginRight: 10

    },
    button: {
        borderWidth: 2,
        borderColor: 'lightseagreen',
        borderRadius: 25,
        width: 145,
        height: 50,
        marginTop: 20,


        justifyContent: 'center',
        fontWeight: '600',
        marginHorizontal: 20,

    },
});