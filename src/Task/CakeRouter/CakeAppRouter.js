import React, { Component, useEffect } from 'react';
import {
  NavigationContainer,
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';


import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BackIcon from 'react-native-vector-icons/SimpleLineIcons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MainTabScreen from './MainTabScreen';
import ProfileStackScreen from './MainTabScreen';
import HomeStackScreen from './MainTabScreen';
import Notifications from '../Notifications';
import Settings from '../Settings';

import { Provider as PaperProvider, DefaultTheme as PaperDefaultTheme, DarkTheme as PaperDarkTheme, DarkTheme } from 'react-native-paper';

import { DrawerContent } from './DrawerContent';
import Profile from '../Profile';



const Drawer = createDrawerNavigator();



const CakeAppRouter = ({ navigation }) => {

  return (

    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          drawerActiveBackgroundColor: 'purple',
          drawerActiveTintColor: '#fff',
          drawerStyle: {
            //backgroundColor: '#c6cbef',
            width: 240,

          },
          drawerLabelStyle: { marginLeft: -10 }
        }}

        drawerContent={props => <DrawerContent {...props} />} >
        <Drawer.Screen name="Home" component={MainTabScreen}
          options={{
            drawerIcon: ({ color }) => (
              <MaterialCommunityIcons
                name='home'
                color={color}
                size={22}

              />
            )
          }}
        />
        <Drawer.Screen name="Profile" component={ProfileStackScreen}
          options={{
            drawerIcon: ({ color }) => (
              <MaterialCommunityIcons
                name='account'
                color={color}
                size={22}

              />
            )
          }}
        />
        <Drawer.Screen name="Notifications" component={Notifications}
          options={{
            drawerIcon: ({ color }) => (
              <Icon
                name='notifications'
                color={color}
                size={22}

              />
            )
          }}
        />
        <Drawer.Screen name="Settings" component={Settings}
          options={{
            drawerIcon: ({ color }) => (
              <Icon
                name='settings'
                color={color}
                size={22}

              />
            )
          }}
        />
      </Drawer.Navigator>

    </NavigationContainer>

  );
};


export default CakeAppRouter;
