import React, { Component, useState } from 'react';
import { StyleSheet, View, Image, ImageBackground } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem
} from '@react-navigation/drawer';
import {
    Avatar,
    Title,
   DarkTheme,
    Caption,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
    useTheme,
   ToggleButton

} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { color } from 'react-native-reanimated';

export function DrawerContent(props) {

    

    const [isDarkTheme,setIsDarkTheme]= React.useState(false);
    

   
    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props} >
                
                <ImageBackground 
                source={{uri:'https://ak.picdn.net/shutterstock/videos/8385943/thumb/1.jpg'}} 
                style={{padding:20,marginTop:-5}}>
                    <View style={{flexDirection:'row'}}>
                        <View>
                     <Image 
                          source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoS6e4wEUPk1Wu66ifYgy-mVW7ICFKVlp0IT_b6dsDBjibNtqExcOEsF16-dVLLQBkBq8&usqp=CAU'}}
                         
                        style={{borderRadius:40,height:80,width:80,resizeMode:'center'}}
                          />
                          <View style={{paddingLeft:7,marginTop:5}}>
                          <Title style={{fontSize:16,marginTop:5,fontWeight:'bold',color:'#fff'}}>Janani I</Title>
                          <Caption style={{fontSize:14,color:'#fff'}}>@goodvibesonly</Caption>
                          </View>
                          </View>
                          <View style={{marginLeft:50,marginTop:0}}>
                          <MaterialCommunityIcons.Button name="close" size={22} color = 'white' backgroundColor='transparent' onPress={() => { props.navigation.closeDrawer(); }} />
                          </View>
                          </View>
                    </ImageBackground>
                
              <View style={{flex:1,paddingTop:10}}>
               <DrawerItemList {...props}/>
               </View>
               <View>
               <Drawer.Section style={{marginTop:20,borderTopWidth:1,borderTopColor:'#f4f4f4'}}>
                     <TouchableRipple onPress={() =>{toggleTheme()}}>
                         <View style={{flexDirection:'row',justifyContent:'space-between',paddingVertical:30,paddingHorizontal:16,marginLeft:10}}>
                             <Text>Dark Theme</Text>
                             <View pointerEvents="none">
                             <Switch value={isDarkTheme}/>
                             </View>
                         </View>
                     </TouchableRipple>
                  </Drawer.Section>
               </View>
               {/* <View style={{flex:1}}>
                  <View style={{paddingLeft:20,marginTop:12,flexDirection:'row'}}>
                      <View>
                          <Image 
                          source={{uri:'https://as1.ftcdn.net/v2/jpg/01/13/41/66/500_F_113416666_a7CuS6cvc6D5P5ezUbsTMexJHm9iAgga.jpg'}}
                          
                        style={{borderRadius:40,height:80,width:80}}
                          />
                     
                          <Title style={{fontSize:16,marginTop:5,fontWeight:'bold'}}>Janani I</Title>
                          <Caption style={{fontSize:14}}>@goodvibesonly</Caption>
                      </View>
                      <View style={{marginLeft:50,marginTop:5}}>
                      <MaterialCommunityIcons.Button name="menu" size={23} color = 'black' backgroundColor={'#fff'} onPress={() => { props.navigation.closeDrawer(); }} />
                      </View>
                  </View>
                  <Drawer.Section style={{marginTop:15}}>
                  <DrawerItem
                  
                    icon={({ color, size }) => (
                        <Icon
                            name='home'
                            color={color}
                            size={size}

                        />
                    )}
                    label="Home"
                    onPress={() => {props.navigation.navigate('HomeScreen')} }
                    
                />
                 <DrawerItem
                    icon={({ color, size }) => (
                        <MaterialCommunityIcons
                            name='account'
                            color={color}
                            size={size}

                        />
                    )}
                    label="Profile"
                    onPress={() => {props.navigation.navigate('ProfileScreen')} }
                />
                 <DrawerItem
                    icon={({ color, size }) => (
                        <Icon
                            name='notifications'
                            color={color}
                            size={size}

                        />
                    )}
                    label="Notifications"
                    onPress={() => {props.navigation.navigate('Notifications')} }
                />
                 <DrawerItem
                    icon={({ color, size }) => (
                        <Icon
                            name='settings'
                            color={color}
                            size={size}

                        />
                    )}
                    label="Settings"
                    onPress={() => {props.navigation.navigate('Settings')} }
                />
                  </Drawer.Section>
                  <Drawer.Section>
                     <TouchableRipple onPress={() =>{toggleTheme()}}>
                         <View style={{flexDirection:'row',justifyContent:'space-between',paddingVertical:30,paddingHorizontal:16,marginLeft:10}}>
                             <Text>Dark Theme</Text>
                             <View pointerEvents="none">
                             <Switch value={isDarkTheme}/>
                             </View>
                         </View>
                     </TouchableRipple>
                  </Drawer.Section>
                    </View> */}
                    </DrawerContentScrollView> 
            <Drawer.Section style={{borderTopColor:'#f4f4f4',borderTopWidth:1,marginBottom:15}}>
                <DrawerItem
                    icon={({ color, size }) => (
                        <Icon
                            name='logout'
                            color={color}
                            size={size}

                        />
                    )}
                    label="Sign Out"
                    onPress={() => {} }
                />
            </Drawer.Section>
        </View>

    );
}