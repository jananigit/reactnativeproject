import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BackIcon from 'react-native-vector-icons/SimpleLineIcons';

import Flatlist from '../../Pages/Flatlist';
import Cake from '../Cake';
import Profile from '../Profile';
import Cart from '../Cart';

const HomeStack = createNativeStackNavigator();
const ProfileStack = createNativeStackNavigator();
const CartStack = createNativeStackNavigator();

const Tab = createMaterialBottomTabNavigator();


const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="HomeScreen"
    activeColor="#fff"
    shifting={true}
    //labeled={false}
    barStyle={{ backgroundColor: '#2A5CE6' }}
  >
    <Tab.Screen
      name="HomeScreen"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarColor: '#2A5CE6',
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="CartScreen"
      component={CartStackScreen}
      options={{
        tabBarLabel: 'Cart',
        tabBarColor: '#7C46C1',
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="cart" color={color} size={26} />
        ),
    
      }}
    />
    <Tab.Screen
      name="ProfileScreen"
      component={ProfileStackScreen}
      options={{
        tabBarLabel: 'Profile',
        tabBarColor: '#CD216D',
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="account" color={color} size={26} />
        ),
      }}
    />

  </Tab.Navigator>

);

export default MainTabScreen;


const HomeStackScreen = ({ navigation }) => (
  <HomeStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#2A5CE6',
      elevation: 0,
      shadowOpacity: 0
    }

  }}>
    <HomeStack.Screen
      name="Home Screen"
      component={Cake}
      options={{
        title: 'Home',
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: 'bold',
          textAlign: 'center',

        },
        headerLeft: () => (
          <MaterialCommunityIcons.Button name="menu" size={20} backgroundColor={'#2A5CE6'} onPress={() => { navigation.openDrawer(); }} />
        ),

      }}
    />


  </HomeStack.Navigator>
);
const ProfileStackScreen = ({ navigation }) => (
  <ProfileStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#2A5CE6',
      elevation: 0,
      shadowOpacity: 0

    }
  }}>

    <ProfileStack.Screen
      name="Profile Screen" component={Profile} options={{
        title: 'Profile',
        headerStyle: {
          backgroundColor: '#CD216D',
        },
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: 'bold',
          textAlign: 'center'
        },
        headerLeft: () => (
          <MaterialCommunityIcons.Button name="menu" size={20} backgroundColor={'#CD216D'} onPress={() => { navigation.openDrawer(); }} />
        ),
      }} />
  </ProfileStack.Navigator>

);

const CartStackScreen = ({ navigation }) => (
  <CartStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#2A5CE6',
      elevation: 0,
      shadowOpacity: 0

    }
  }}>

    <CartStack.Screen
      name="Cart Screen" component={Cart} options={{
        title: 'Cart',
        headerStyle: {
          backgroundColor: '#7C46C1',
        },
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: 'bold',
          textAlign: 'center'
        },
        headerLeft: () => (
          <MaterialCommunityIcons.Button name="menu" size={20} backgroundColor={'#7C46C1'} onPress={() => { navigation.openDrawer(); }} />
        ),
      }} />
  </CartStack.Navigator>

);





