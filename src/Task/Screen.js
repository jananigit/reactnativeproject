import React, {Component} from 'react';  
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView} from 'react-native';  



const Screen = () =>{

    return(
        <SafeAreaView style = {styles.container}>
        <View style={{flex:1,marginVertical:45}}>
            <Text  style= {{fontSize:30, fontWeight:'bold',color: 'slateblue',textAlign:'center'}}>BrainBob</Text>
            <Image 
            style={{height:300, width:300,margin:20}}
            source={require('../../assets/screen.png')}
            />
            
           
        </View>
        <View style={styles.txtContainer}>
            <Text style= {{fontSize:30, fontWeight:'bold',color: 'black',textAlign:'center'}}>Be ready to learn English easily</Text>
            <Text style={{fontSize: 15, marginTop: 20, textAlign: 'center'}}>Listen to stories, watch videos and improve your language with BrainBob</Text>
          

           <TouchableOpacity>
                <Text style={[styles.button,{marginTop:45, fontSize:18, color: 'white', backgroundColor: 'slateblue'}]}>Get Started</Text>
            </TouchableOpacity>

        </View>



        

    </SafeAreaView>
    )
};
export default Screen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
    },
    txtContainer: {
       
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 80,
        marginHorizontal:20
    }, 
    
    button: {
        borderWidth:2,
        borderColor: 'slateblue',
        borderRadius: 24,
        width: 240,
        height: 50,
        textAlign: 'center',
       padding: 12,
       fontWeight: '600',
       justifyContent:'center'
       
    }
});

