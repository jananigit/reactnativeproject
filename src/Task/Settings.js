import React, {Component} from 'react';  
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView} from 'react-native';  
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


const Settings = ({navigation}) => {
    return(
        <View style={{flex:1}}>
            
            <MaterialCommunityIcons.Button name="menu" size={25}  onPress={() => { navigation.openDrawer(); }} />
           
            <View style={{alignItems:'center',justifyContent:'center',flex:1}}>
            <Text>Settings Screen</Text>
            </View>
        </View>
    )
};
export default Settings;