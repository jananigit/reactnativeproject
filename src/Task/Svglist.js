import React, { useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { Avatar } from 'react-native-paper';

import WavyHeader from './WavyHeader';




const Svglist = () => {

    const [isLoading, setLoading] = useState(true);

    const [data, setData] = useState([]);

    const getList = async () => {
        try {
            const response = await fetch('https://reqres.in/api/users?page=1');
            const json = await response.json();
            setData(json);
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getList();
    }, []);



    return (
        <SafeAreaView style={styles.container} >


            <WavyHeader />
            <View style={{ position: 'absolute', top: 3, flex: 0.75, zIndex: 1 }}>
                <View style={{ flexDirection: 'row', margin: 30, marginTop: 8 }}>

                    <Text style={{ color: '#fff', fontSize: 22, fontWeight: '600' }}>Winners</Text>


                    <Text style={{ color: '#fff', marginLeft: 20, marginTop: 6 }}>Friends</Text>
                    <EvilIcons name='chevron-down' size={22} color='white' style={{ marginTop: 7 }} />
                    <Ionicons name='person-add-outline' size={23} color='white' style={{ marginLeft: 90 }} />
                    <Ionicons name='search' size={24} color='white' style={{ marginLeft: 18 }} />

                </View>
                <View style={styles.itemrow}>

                    <TouchableOpacity style={styles.today}>
                        <Text style={{ marginHorizontal: 18, color: '#201494', fontWeight: '700', fontSize: 15, }}>Today</Text>

                    </TouchableOpacity>
                    <TouchableOpacity style={styles.month}>
                        <Text style={{ marginHorizontal: 15, color: '#fff', fontSize: 15, }}>Month</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.alltime}>
                        <Text style={{ color: '#fff', marginLeft: -25, fontSize: 15, }}>All Time</Text>
                    </TouchableOpacity>





                </View>
                <View style={{ flexDirection: 'row', marginTop: 12 }}>
                    <View>
                        <View style={styles.avatarm}>
                            <Avatar.Image source={{ uri: 'https://reqres.in/img/faces/2-image.jpg' }} size={60} />
                        </View>
                        <View>
                            <Image source={{ uri: 'http://assets.stickpng.com/thumbs/580b585b2edbce24c47b2af3.png' }} style={{ height: 40, width: 45, marginTop: -24, marginLeft: 45 }} />
                            <Text style={{ marginTop: -22, marginLeft: 64, fontWeight: '800' }}>2</Text>
                            <Text style={[styles.txtop, { marginLeft: 40 }]}>Jannet</Text>
                            <Text style={[styles.txtbottom, { marginLeft: 43 }]}>6500 pts</Text>
                        </View>
                    </View>
                    <View>
                        <View style={styles.avatarb}>
                            <Avatar.Image source={{ uri: 'https://reqres.in/img/faces/1-image.jpg' }} size={80} />
                        </View>
                        <View>
                            <Image source={{ uri: 'http://assets.stickpng.com/thumbs/580b585b2edbce24c47b2af3.png' }} style={{ height: 45, width: 50, marginTop: -24, marginLeft: 35 }} />
                            <Text style={{ marginTop: -24, marginLeft: 56, fontWeight: '800' }}>1</Text>
                            <Text style={[styles.txtop, { marginLeft: 35 }]}>George</Text>
                            <Text style={[styles.txtbottom, { marginLeft: 38 }]}>7400 pts</Text>
                        </View>
                    </View>
                    <View>

                        <View style={styles.avatars}>
                            <Avatar.Image source={{ uri: 'https://reqres.in/img/faces/3-image.jpg' }} size={60} />
                        </View>
                        <View>
                            <Image source={{ uri: 'http://assets.stickpng.com/thumbs/580b585b2edbce24c47b2af3.png' }} style={{ height: 35, width: 40, marginTop: -25, marginLeft: 28 }} />
                            <Text style={{ marginTop: -21, marginLeft: 44, fontWeight: '800' }}>3</Text>
                            <Text style={[styles.txtop, { marginLeft: 30 }]}>Emma</Text>
                            <Text style={[styles.txtbottom, { marginLeft: 30 }]}>5800 pts</Text>
                        </View>
                    </View>
                </View>
            </View>


            <View style={{ backgroundColor: '#fff', flex: 0.2 }} ></View>

            <View style={{ flex: 1, backgroundColor: '#DCDBE9', marginTop: -30 }}>
                {isLoading ? <ActivityIndicator size="large" color="#2046E7" /> : (
                    <FlatList
                        data={data.data}
                        renderItem={({ item }) => (
                            <SafeAreaView>
                                <View style={styles.flatlist}>
                                    <Text style={styles.flatid} >{item.id} </Text>
                                    <Image style={styles.flatimg} source={{ uri: item.avatar }} />
                                    <Text style={styles.flattxt} >{item.first_name}  {item.last_name} </Text>
                                    <Text style={styles.point} >7400 pts</Text>
                                </View>
                            </SafeAreaView>
                        )}

                    />
                )}



            </View>
            <View style={{ height: 60, flexDirection: 'row'}}>
                <MaterialIcons name='home' size={30} style={{ marginHorizontal: 20, marginVertical: 15 }} />
                <MaterialCommunityIcons name='cog-counterclockwise' size={32} style={{ marginHorizontal: 20, marginVertical: 15 }} />
                <Avatar.Image source={{ uri: 'https://reqres.in/img/faces/3-image.jpg' }} size={35} style={{ marginHorizontal: 20, marginVertical: 15, backgroundColor: 'transparent' }}></Avatar.Image>
                <MaterialIcons name='bar-chart' size={52} style={styles.maticon} />
                <SimpleLineIcons name='bell' size={24} style={{ position: 'absolute', right: 20, top: 22 }} />

            </View>
        </SafeAreaView>
    );
};

export default Svglist;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DCDBE9'
    },


    itemrow: {
        borderRadius: 25,
        backgroundColor: '#6F67BF',
        height: 42,
        marginHorizontal: 12,
        flexDirection: 'row'
    },

    today: {
        backgroundColor: 'white',
        borderRadius: 25,
        width: 120,
        alignItems: 'center',
        justifyContent: 'center'
    },

    month: {
        color: 'white',
        borderRadius: 25,
        width: 120,
        alignItems: 'center',
        justifyContent: 'center'
    },

    alltime: {
        color: 'white',
        borderRadius: 25,
        width: 120,
        alignItems: 'center',
        justifyContent: 'center'
    },

    avatarm: {
        margin: 18,
        marginLeft: 33,
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 60,
        height: 66,
        zIndex: 1
    },

    avatarb: {
        margin: 20,
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 60,
        height: 86,
        marginLeft: 17,
        marginTop: 0,
        zIndex: 1
    },

    avatars: {
        margin: 20,
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 60,
        height: 66,
        marginLeft: 14,
        zIndex: 1
    },

    txtop: {
        color: 'white',
        marginLeft: 40,
        marginTop: 5,
        fontSize: 16,
        fontWeight: '600'
    },

    txtbottom: {
        color: 'white',
        marginLeft: 38,
        fontSize: 11,
        fontWeight: '300'
    },


    flatlist: {
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 5,
        marginBottom: 15,
        marginRight: 8,
        height: 60,
        borderTopRightRadius: 35,
        borderBottomRightRadius: 35,
        borderWidth: 2,
        borderColor: '#fff',
        hadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 5,
        shadowColor: '#201494'
    },
    maticon: {
        marginHorizontal: 20,
        marginVertical: 0,
        color: 'red',
        borderTopWidth: 4,
        borderTopColor: 'red',
        width: 55,
        backgroundColor: 'tomato',
        opacity: 0.3
    },

    flatid: {
        marginVertical: 12,
        marginLeft: 10,
        color: '#4b0082',
        fontWeight: '600',
        fontSize: 15
    },

    flatimg: {
        height: 50,
        width: 50,
        resizeMode: 'center',
        borderRadius: 30,
        marginHorizontal: 10
    },

    flattxt: {
        marginVertical: 12,
        color: '#4b0082',
        fontWeight: '600',
        fontSize: 15
    },

    point: {
        marginVertical: 13,
        position: 'absolute',
        right: 15,
        top: 3
    },
    svgCurve: {
        position: 'absolute',

    },
});
