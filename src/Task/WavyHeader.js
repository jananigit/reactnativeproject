import React from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

export default function WavyHeader({ customStyles }) {
    return (
      <View style={customStyles}>
        <View style={{ backgroundColor: '#201494', height: 280,zIndex:1 }}>
         <Svg
            height="200%"
            width="100%"
            viewBox="322 5 1125 230"
            style={{ position: 'absolute', top: 30, zIndex:1 }}
          >
            <Path
              fill="#201494"
              d="M0,64L48,85.3C96,107,192,149,288,181.3C384,213,480,235,576,213.3C672,192,768,128,864,101.3C960,75,1056,85,1152,112C1248,139,1344,181,1392,202.7L1440,224L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
            />
         </Svg> 


        </View>
      </View>
    );
  }